package advisor.menu.handler;

import advisor.api.contollers.AuthConfiguration;
import advisor.menu.RequestHandler;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.requests.data.browse.GetCategorysPlaylistsRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CategoriesHandler implements RequestHandler {

  // TODO: pass category name to request. Now it static = "party";
  private String categoryId;

  public CategoriesHandler(String category) {
    this.categoryId = category;
  }

  private final GetCategorysPlaylistsRequest request =
      AuthConfiguration.spotifyApi.getCategorysPlaylists("party")
          .limit(10)
          .build();

  /**
   * print names in play list of category
   */
  @Override
  public void getAsync() {
    try {
      final Future<Paging<PlaylistSimplified>> pagingFuture = request.executeAsync();

      final Paging<PlaylistSimplified> playList = pagingFuture.get();


      for (PlaylistSimplified entry : playList.getItems()
      ) {
        System.out.println("Name: " + entry.getName());
      }

    } catch (InterruptedException | ExecutionException e) {
      System.out.println("Error: " + e.getCause().getMessage());
    }
  }
}
