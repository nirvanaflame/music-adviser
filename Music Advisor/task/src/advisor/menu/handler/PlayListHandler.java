package advisor.menu.handler;

import static java.util.Arrays.stream;

import advisor.api.contollers.AuthConfiguration;
import advisor.menu.RequestHandler;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsTracksRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class PlayListHandler implements RequestHandler {

  private String playListName;

  public PlayListHandler(String name) {
    this.playListName = name;
  }


  @Override
  public void getAsync() {
    try {
      final GetPlaylistsTracksRequest playlistsTracksRequest =
          AuthConfiguration.spotifyApi.getPlaylistsTracks(playListName).build();
      final Future<Paging<PlaylistTrack>> pagingFuture = playlistsTracksRequest.executeAsync();
      final Paging<PlaylistTrack> playlistTrack = pagingFuture.get();

      stream(playlistTrack.getItems()).forEach(it -> System.out.println(it.getTrack().getName()));

    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
  }
}
