package advisor;


import advisor.api.contollers.AuthConfiguration;
import advisor.menu.Menu;
import advisor.menu.RequestHandler;
import advisor.menu.handler.CategoriesHandler;
import advisor.menu.handler.FeaturedHandler;
import advisor.menu.handler.NewAlbumsHandler;
import advisor.menu.handler.PlayListHandler;
import com.google.gson.Gson;
import java.util.Scanner;


public class Main {

  private static Scanner scanner = new Scanner(System.in);
  private static RequestHandler requestHandler;
  private static Menu menu = new Menu();


  public static void main(String[] args) {

    while (!menu.isClosed()) {
      String input = scanner.nextLine();

      if (input.equals("auth")) {
        System.out.println("---Authorization---");
        AuthConfiguration.clientCredentialsAsync();
        menu.setAuthorized();
        System.out.println("---SUCCESS---");
      }

      if (!menu.isAuthorized()) {
        System.out.println("Please, provide access for application.");
      } else {
        if (input.equals("new")) {
          System.out.println("---NEW RELEASES---");
          requestHandler = new NewAlbumsHandler();
          requestHandler.getAsync();

        }
        if (input.equals("feat")) {
          System.out.println("---FEATURED---");
          requestHandler = new FeaturedHandler();
          requestHandler.getAsync();

        }
        if (input.equals("cat")) {
          System.out.println("---CATEGORIES---");
          String category = scanner.nextLine();
          requestHandler = new CategoriesHandler(category);
          requestHandler.getAsync();

        }
        if (input.contains("playlist")) {
          String playListName = input.substring(9);
          System.out.printf("---%S PLAYLISTS---\n", playListName.toUpperCase());

          requestHandler = new PlayListHandler(playListName);
          requestHandler.getAsync();
        }
      }

      if (input.equals("exit")) {
        System.out.println("---GOODBYE!---");
        menu.setClosed();
      }
    }
  }

}
