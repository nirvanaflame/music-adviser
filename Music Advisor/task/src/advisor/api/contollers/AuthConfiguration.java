package advisor.api.contollers;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AuthConfiguration {

  private static final String clientId = "16319e7bcb784da3b234e04aeac0d092";
  private static final String clientSecret = "68824774c5d843478a5094363e307a33";

  public static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
      .setClientId(clientId)
      .setClientSecret(clientSecret)
      .build();

  private static final ClientCredentialsRequest clientCredentialsRequest = spotifyApi
      .clientCredentials()
      .build();

  /**
   * Set access token for further "spotifyApi" object usage.
   */
  public static void clientCredentialsAsync() {
    try {
      final Future<ClientCredentials> clientCredentialsFuture = clientCredentialsRequest
          .executeAsync();

      final ClientCredentials clientCredentials = clientCredentialsFuture.get();

      spotifyApi.setAccessToken(clientCredentials.getAccessToken());

      System.out.println("Expire in: " + clientCredentials.getExpiresIn()/60 + " minutes.");
      System.out.println("token: " + clientCredentials.getAccessToken());
      System.out.println("token type: " + clientCredentials.getTokenType());

    } catch (InterruptedException | ExecutionException e) {
      System.out.println("Error: " + e.getCause().getMessage());
    }
  }
}
