package advisor.menu.handler;

import advisor.api.contollers.AuthConfiguration;
import advisor.menu.RequestHandler;
import com.wrapper.spotify.model_objects.special.FeaturedPlaylists;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.requests.data.browse.GetListOfFeaturedPlaylistsRequest;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Stream;

public class FeaturedHandler implements RequestHandler {

  private final GetListOfFeaturedPlaylistsRequest featuredPlaylistsRequest =
      AuthConfiguration.spotifyApi.getListOfFeaturedPlaylists().build();

  @Override
  public void getAsync() {
    try {
      final Future<FeaturedPlaylists> playlistsFuture = featuredPlaylistsRequest.executeAsync();
      final FeaturedPlaylists playlists = playlistsFuture.get();

      Stream<PlaylistSimplified> stream = Arrays.stream(playlists.getPlaylists().getItems());

      stream.forEach(it -> System.out.println(it.getName()));

    } catch (InterruptedException | ExecutionException e) {
      System.out.println("Error: " + e.getCause().getMessage());
    }

  }
}
