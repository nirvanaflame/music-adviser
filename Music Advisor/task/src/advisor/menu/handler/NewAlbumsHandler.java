package advisor.menu.handler;

import static java.util.Arrays.stream;

import advisor.api.contollers.AuthConfiguration;
import advisor.menu.RequestHandler;
import com.google.gson.Gson;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.AlbumSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.requests.data.browse.GetListOfNewReleasesRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class NewAlbumsHandler implements RequestHandler {

  private final GetListOfNewReleasesRequest getListOfNewReleasesRequest = AuthConfiguration.spotifyApi.getListOfNewReleases()
      .limit(10)
      .build();

  @Override
  public void getAsync() {
    try {
      final Future<Paging<AlbumSimplified>> albumFuture = getListOfNewReleasesRequest.executeAsync();

      final Paging<AlbumSimplified> albumList = albumFuture.get();

      for (AlbumSimplified entry: albumList.getItems()
      ) {
        System.out.print(entry.getName() + " [");
            stream(entry.getArtists()).forEach(artist -> System.out.print(artist.getName()));
        System.out.println("]");
      }

    } catch (InterruptedException | ExecutionException e) {
      System.out.println("Error: " + e.getCause().getMessage());
    }
  }
}
