package advisor.menu;


public class Menu {
  private boolean isClosed = false;
  private boolean isAuthorized = false;

  public boolean isAuthorized() {
    return isAuthorized;
  }

  public void setAuthorized() {
    isAuthorized = true;
  }

  public boolean isClosed() {
    return isClosed;
  }

  public void setClosed() {
    isClosed = true;
  }
}
